
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Amdreas
 */
public class Pratica43 {

    public static void main(String[] args) {
        Quadrado q = new Quadrado(3.0);
        Retangulo r = new Retangulo(3.0, 5.0);
        TrianguloEquilatero t = new TrianguloEquilatero(4.0);
        System.out.println("Area do " + q.getNome() + " = " + q.getArea());
        System.out.println("Perimetro " + q.getNome() + " = " + q.getPerimetro());
        System.out.println("Area do " + r.getNome() + " = " + r.getArea());
        System.out.println("Perimetro " + r.getNome() + " = " + r.getPerimetro());
        System.out.println("Area do " + t.getNome() + " = " + t.getArea());
        System.out.println("Perimetro " + t.getNome() + " = " + t.getPerimetro());
    }

}
